package sbp.exception;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import sbp.exceptions.CustomException;
import sbp.exceptions.WorkWithExceptions;

public class WorkWithExceptionTest {

    /**
     * Тестирование метода на пробрасование нашего кастомного исключения {@link CustomException}
     * @throws Exception
     */
    @Test
    public void exceptionProcessingTest() throws Exception
    {
        WorkWithExceptions workWithExceptions = new WorkWithExceptions();
        Throwable throwable = Assertions.assertThrows(CustomException.class, () -> { workWithExceptions.exceptionProcessing(); });
    }
}