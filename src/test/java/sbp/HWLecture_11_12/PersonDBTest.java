package sbp.HWLecture_11_12;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import sbp.HWLecture_7_8.Person;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class PersonDBTest {

    /**
     * Проверка результата выполнения метода добавления нового пользователя
     */
    @Test
    public void addPerson_success_Test() {
        Person person = new Person("TestName", "TestCity", 99);
        PersonDB personDB = PersonDB.getInstance();
        boolean result = personDB.addPerson(person);

        Assertions.assertTrue(result);
    }

    /**
     * Проверка результата выполнения метода в случае если таблицы не существует
     */
    @Test
    public void addPerson_fail_Test() {
        Person person = new Person("Test", "Test", 99);
        PersonDB personDB = PersonDB.getInstance();
        boolean result = personDB.addPerson(person);

        Assertions.assertFalse(result);
    }

    /**
     * Проверка результата выполнения метода удаления существующей таблицы
     */
    @Test
    public void dropTable_Test() {

        PersonDB personDB = PersonDB.getInstance();
        boolean result = personDB.dropTable();

        Assertions.assertTrue(result);
    }

    /**
     * Проверка результата выполнения метода на существование записи в таблице
     */
    @Test
    public void personExist_success_Test() {

        PersonDB personDB = PersonDB.getInstance();
        Person testPerson = new Person("Test", "Test", 99);
        personDB.addPerson(testPerson);
        boolean exist = personDB.personExist(testPerson);

        Assertions.assertTrue(exist);
    }

    /**
     * Проверка результата выполнения метода на существование записи в таблице
     */
    @Test
    public void personExist_fail_Test() {

        PersonDB personDB = PersonDB.getInstance();
        personDB.deletePerson(99);
        boolean exist = personDB.personExist(new Person("Test", "Test", 99));

        Assertions.assertFalse(exist);
    }
}
