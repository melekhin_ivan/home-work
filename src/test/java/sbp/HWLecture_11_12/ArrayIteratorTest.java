package sbp.HWLecture_11_12;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class ArrayIteratorTest {

    /**
     * Проверка работоспособности метода
     */
    @Test
    public void hasNext_true_Test() {

        Integer[][] array = new Integer[][]{{1}, {2}};
        ArrayIterator i = new ArrayIterator(array);

        Assertions.assertTrue(i.hasNext());
    }

    /**
     * Проверка работоспособности метода
     */
    @Test
    public void hasNext_false_Test() {

        Integer[][] array = new Integer[0][0];
        ArrayIterator i = new ArrayIterator(array);

        Assertions.assertFalse(i.hasNext());
    }

    /**
     * Проверка работоспособности метода
     */
    @Test
    public void next_Test() {

        Integer[][] array = new Integer[][]{{1,2}, {3,4}};
        ArrayIterator i = new ArrayIterator(array);

        Assertions.assertEquals(1,i.next());
        Assertions.assertEquals(2,i.next());
        Assertions.assertEquals(3,i.next());
        Assertions.assertEquals(4,i.next());
    }

    /**
     * Проверка работоспособности метода при выходе за границы массива
     */
    @Test
    public void next_exception_Test() {

        Integer[][] array = new Integer[][]{{1}, {2}};
        ArrayIterator i = new ArrayIterator(array);

        Assertions.assertEquals(1,i.next());
        Assertions.assertEquals(2,i.next());
        Assertions.assertThrows(ArrayIndexOutOfBoundsException.class, () -> i.next());
    }
}
