package sbp.HWLecture_11_12;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import sbp.io.MyIOExample;

import java.io.FileNotFoundException;
import java.io.IOException;

public class MyIOExampleNIO2Test {

    /**
     * Проверка результата выполнения метода в зависимости от существования файла
     */
    @Test
    public void workWithFile_Test() throws IOException {

        MyIOExampleNIO2 myIOExample = new MyIOExampleNIO2();

        Assertions.assertTrue(myIOExample.workWithFile("TestMIO_new.txt"));
        Assertions.assertFalse(myIOExample.workWithFile("TestMIO.txt"));
    }

    /**
     * Проверка результата выполнения метода в зависимости от существования файла
     */
    @Test
    public void copyFile_Test() throws IOException {

        MyIOExampleNIO2 myIOExample = new MyIOExampleNIO2();
        try {
            Assertions.assertFalse(myIOExample.copyFile("File_not_exists", "File2"));
        } catch (FileNotFoundException e) {
            System.out.println("Error: " + e.getMessage());
        }

        Assertions.assertTrue(myIOExample.copyFile("TestMIO.txt", "TestMIO2.txt"));
    }

    /**
     * Проверка результата выполнения метода в зависимости от существования файла
     */
    @Test
    public void copyBufferedFile_Test() throws IOException {

        MyIOExampleNIO2 myIOExample = new MyIOExampleNIO2();
        try {
            Assertions.assertFalse(myIOExample.copyBufferedFile("File_not_exists", "File2"));
        } catch (FileNotFoundException e) {
            System.out.println("Error: " + e.getMessage());
        }

        Assertions.assertTrue(myIOExample.copyBufferedFile("TestMIO.txt", "TestMIO2.txt"));
    }

    /**
     * Проверка результата выполнения метода в зависимости от существования файла
     */
    @Test
    public void copyFileWithReaderAndWriter_Test() throws IOException {

        MyIOExampleNIO2 myIOExample = new MyIOExampleNIO2();
        try {
            Assertions.assertFalse(myIOExample.copyFileWithReaderAndWriter("File_not_exists", "File2"));
        } catch (FileNotFoundException e) {
            System.out.println("Error: " + e.getMessage());
        }

        Assertions.assertTrue(myIOExample.copyFileWithReaderAndWriter("TestMIO.txt", "TestMIO2.txt"));
    }
}

