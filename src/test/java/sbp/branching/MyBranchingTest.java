package sbp.branching;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import sbp.common.Utils;

public class MyBranchingTest {

    /**
     * Проверка результата выполнения метода в зависимости от возможных реализаций {@link Utils}
     * Utils#utilFunc2() возвращает true
     */
    @Test
    public void ifElseExampleTest_Success()
    {
        Utils utilsTest = Mockito.mock(Utils.class);
        Mockito.when(utilsTest.utilFunc2()).thenReturn(true);

        MyBranching myBranching = new MyBranching(utilsTest);

        boolean res = myBranching.ifElseExample();
        Assertions.assertTrue(res, "Я проверил, будет ли рузальтат равен true");
    }

    /**
     * Проверка результата выполнения метода в зависимости от возможных реализаций {@link Utils}
     * Utils#utilFunc2() возвращает false
     */
    @Test
    public void ifElseExampleTest_Fail ()
    {
        Utils utilsTest = Mockito.mock(Utils.class);
        Mockito.when(utilsTest.utilFunc2()).thenReturn(false);

        MyBranching myBranching = new MyBranching(utilsTest);

        boolean res2 = myBranching.ifElseExample();
        Assertions.assertFalse(res2, "Я проверил, будет ли рузальтат равен false");
    }

    /**
     * Проверка результата выполнения метода в зависимости от возможных реализаций {@link Utils}
     * Ожидается 0, если Utils#utilFunc2 вернёт true;
     */
    @Test
    public void maxIntTest_true ()
    {
        int i1 = 5;
        int i2 = 7;

        Utils utilsTest = Mockito.mock(Utils.class);
        Mockito.when(utilsTest.utilFunc2()).thenReturn(true);

        MyBranching myBranching = new MyBranching(utilsTest);
        int result = myBranching.maxInt(i1, i2);
        System.out.println("Если true, то возвращается 0, результат: " + result);
        Assertions.assertEquals(0, myBranching.maxInt(i1, i2));
    }

    /**
     * Проверка результата выполнения метода в зависимости от возможных реализаций {@link Utils}
     * Ожидается большее из значений, если Utils#utilFunc2 вернёт false;
     */
    @Test
    public void maxIntTest_false ()
    {
        int i1 = 5;
        int i2 = 7;
        Utils utilsTest = Mockito.mock(Utils.class);
        Mockito.when(utilsTest.utilFunc2()).thenReturn(false);

        MyBranching myBranching = new MyBranching(utilsTest);
        int result2 = myBranching.maxInt(i1, i2);
        System.out.println("Если false, то возвращается наибольшее из переданых чисел, результат: " + result2);
        Assertions.assertEquals(Math.max(i1, i2), myBranching.maxInt(i1, i2));
    }

    /**
     * Проверка количества обращений к {@link Utils}
     * При входящем i = 0
     * Utils#utilFunc2() возвращает true
     */
    @Test
    public void switchExampleTest_0_True () {

        Utils utilsTest = Mockito.mock(Utils.class);

        Mockito.when(utilsTest.utilFunc2()).thenReturn(true);
        Mockito.when(utilsTest.utilFunc1(Mockito.anyString())).thenReturn(true);

        MyBranching myBranching = new MyBranching(utilsTest);
        myBranching.switchExample(0);
        Mockito.verify(utilsTest, Mockito.times(1)).utilFunc1("abc2");
        Mockito.verify(utilsTest, Mockito.times(1)).utilFunc2();

    }

    /**
     * Проверка количества обращений к {@link Utils}
     * При входящем i = 0
     * Utils#utilFunc2() возвращает false
     */
    @Test
    public void switchExampleTest_0_False() {

        Utils utilsTest = Mockito.mock(Utils.class);

        Mockito.when(utilsTest.utilFunc2()).thenReturn(false);
        Mockito.when(utilsTest.utilFunc1(Mockito.anyString())).thenReturn(true);

        MyBranching myBranching1 = new MyBranching(utilsTest);
        myBranching1.switchExample(0);
        Mockito.verify(utilsTest, Mockito.never()).utilFunc1("abc2");
        Mockito.verify(utilsTest, Mockito.times(1)).utilFunc2();
    }


    /**
     * Проверка количества обращений к {@link Utils}
     * При входящем i = 1
     */
    @Test
    public void switchExampleTest_1 () {

        Utils utilsTest = Mockito.mock(Utils.class);
        Mockito.when(utilsTest.utilFunc2()).thenReturn(true);
        Mockito.when(utilsTest.utilFunc1(Mockito.anyString())).thenReturn(true);

        MyBranching myBranching = new MyBranching(utilsTest);
        myBranching.switchExample(1);
        Mockito.verify(utilsTest, Mockito.times(1)).utilFunc1("abc");
        Mockito.verify(utilsTest, Mockito.times(1)).utilFunc2();
    }

    /**
     * Проверка количества обращений к {@link Utils}
     * При входящем i = 2
     */
    @Test
    public void switchExampleTest_2 () {

        Utils utilsTest = Mockito.mock(Utils.class);
        Mockito.when(utilsTest.utilFunc2()).thenReturn(true);
        Mockito.when(utilsTest.utilFunc1(Mockito.anyString())).thenReturn(true);

        MyBranching myBranching = new MyBranching(utilsTest);
        myBranching.switchExample(2);
        Mockito.verify(utilsTest, Mockito.never()).utilFunc1(Mockito.anyString());
        Mockito.verify(utilsTest, Mockito.times(1)).utilFunc2();
    }
}
