package sbp.HWLecture_17_18;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import sbp.HWLecture_11_12.PersonDB;
import sbp.HWLecture_7_8.Person;

import java.util.List;


public class RestClientTest {

    /**
     *  Тест работы клиент-серверного приложения
     *  Проверка работы клиента {@link RestClient} и обработки клиентом ответа сервера {@link SimpleRestController}
     */
    @Test
    public void getPerson_Test() {

        App.start();

        ApplicationContext context = new AnnotationConfigApplicationContext("sbp");
        RestClient restClient = context.getBean(RestClient.class);
        PersonDB personDB = context.getBean(PersonDB.class);
        Person person = new Person("ТестИмя", "ТестГород", 1);
        personDB.addPerson(person);

        Assertions.assertEquals(person.getName(),restClient.getPerson(49).getName());

        App.stop(context);
    }

    /**
     *  Тест работы клиент-серверного приложения
     *  Проверка работы клиента {@link RestClient} и обработки клиентом ответа сервера {@link SimpleRestController}
     */
    @Test
    public void postPerson_true_Test() {

        App.start();

        ApplicationContext context = new AnnotationConfigApplicationContext("sbp");
        RestClient restClient = context.getBean(RestClient.class);
        Person person = new Person("ТестИмя", "ТестГород", 1);

        boolean result = restClient.postPerson(person);

        Assertions.assertTrue(result);

        App.stop(context);
    }

    /**
     *  Тест работы клиент-серверного приложения
     *  Проверка работы клиента {@link RestClient} и обработки клиентом ответа сервера {@link SimpleRestController}
     */
    @Test
    public void postPerson_false_Test() {

        App.start();

        ApplicationContext context = new AnnotationConfigApplicationContext("sbp");
        RestClient restClient = context.getBean(RestClient.class);
        Person person = new Person("ТестИмя", "ТестГород", 1);

        boolean result = restClient.postPerson(person);

        Assertions.assertFalse(result);

        App.stop(context);
    }

    /**
     *  Тест работы клиент-серверного приложения
     *  Проверка работы клиента {@link RestClient} и обработки клиентом ответа сервера {@link SimpleRestController}
     */
    @Test
    public void getAllPersons_Test() {

        App.start();

        ApplicationContext context = new AnnotationConfigApplicationContext("sbp");
        RestClient restClient = context.getBean(RestClient.class);
        PersonDB personDB = context.getBean(PersonDB.class);

        List<Person> list = restClient.getAllPersons();

        Assertions.assertEquals(personDB.findAll().toString(), list.toString());

        App.stop(context);
    }
}
