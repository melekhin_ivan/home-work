package sbp.HWLecture_5_6;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import sbp.HWLecture_5_6.collections.CustomArrayImpl;
import sbp.myUser.User;

import java.util.ArrayList;
import java.util.Collection;

public class CustomArrayImpl_test {

    /**
     * Проверка реализации конструктора
     */
    @Test
    public void CustomArrayImpl_Test () {

        CustomArrayImpl<Integer> customArray = new CustomArrayImpl<>();

        Assertions.assertEquals(10, customArray.getCapacity());
        Assertions.assertEquals(0, customArray.size());
    }

    /**
     * Проверка реализации конструктора
     */
    @Test
    public void CustomArrayImpl_Test2() {

        int capacity = 40;

        CustomArrayImpl<Integer> customArray = new CustomArrayImpl<>(capacity);

        Assertions.assertEquals(capacity, customArray.getCapacity());
    }

    /**
     * Проверка реализации конструктора
     */
    @Test
    public void CustomArrayImpl_Test3() {

        Collection<Integer> collection = new ArrayList<>();
        collection.add(7);
        collection.add(8);
        collection.add(9);

        CustomArrayImpl<Integer> customArray = new CustomArrayImpl<>(collection);

        Assertions.assertNotNull(customArray);
        Assertions.assertEquals(3, customArray.size());
    }

    /**
     * Проверка результата выполнения метода в зависимости от возможных реализаций
     */
    @Test
    public void isEmpty_Test() {

        CustomArrayImpl<Integer> customArray = new CustomArrayImpl<>();

        Assertions.assertTrue(customArray.isEmpty());

        customArray.add(13);
        Assertions.assertFalse(customArray.isEmpty());
    }

    /**
     * Проверка результата выполнения метода в зависимости от возможных реализаций
     */
    @Test
    public void addAll_Test() {

        CustomArrayImpl<Integer> customArray = new CustomArrayImpl<>();

        Collection<Integer> collection = new ArrayList<>();
        collection.add(1);
        collection.add(2);
        collection.add(3);

        Assertions.assertTrue(customArray.add(0));

        Assertions.assertTrue(customArray.addAll(collection));

        Integer[] obj = {7,7,7};
        Assertions.assertTrue(customArray.addAll(1, obj));
    }

    /**
     * Проверка результата выполнения метода в зависимости от возможных реализаций
     */
    @Test
    public void set_Test() {

        CustomArrayImpl<Integer> customArray = new CustomArrayImpl<>();

        customArray.add(13);
        customArray.set(0, 31);

        Assertions.assertEquals(31, customArray.set(0, 31));
        Throwable exception = Assertions.assertThrows(ArrayIndexOutOfBoundsException.class, () -> customArray.set(2, 48));
    }

    /**
     * Проверка результата выполнения метода в зависимости от возможных реализаций
     */
    @Test
    public void remove_Test() {

        int index = 1;

        CustomArrayImpl<Integer> customArray = new CustomArrayImpl<>();
        customArray.add(0);
        customArray.add(1);
        customArray.add(2);

        customArray.remove(index);

        Assertions.assertEquals(2, customArray.get(index));

        Throwable exception = Assertions.assertThrows(ArrayIndexOutOfBoundsException.class, () -> customArray.remove(4));
    }

    /**
     * Проверка результата выполнения метода в зависимости от возможных реализаций
     */
    @Test
    public void contains_Test() {

        CustomArrayImpl<Integer> customArray = new CustomArrayImpl<>();
        customArray.add(5232);

        Assertions.assertTrue(customArray.contains(5232));
    }

    /**
     * Проверка результата выполнения метода в зависимости от возможных реализаций
     */
    @Test
    public void indexOf_Test() {

        CustomArrayImpl<String> customArray = new CustomArrayImpl<>();
        customArray.add("A");
        customArray.add("B");
        customArray.add("C");

        Assertions.assertEquals(2, customArray.indexOf("C"));
        Assertions.assertEquals(-1, customArray.indexOf("D"));
    }
}
