package sbp.HWLecture_9_10;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import sbp.HWLecture_7_8.CustomDigitComparator;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Set;

public class FindDuplicatesTest {

    /**
     * Проверка работоспособности метода {@link FindDuplicates}
     */
    @Test
    public void find1_Test1() {

        ArrayList<String> list = new ArrayList<>();
        list.add("6");
        list.add("1");
        list.add("9");
        list.add("99");
        list.add("3");
        list.add("1");
        list.add("6");
        list.add("6");

        FindDuplicates<String> fD = new FindDuplicates<>();
        Object[] result = fD.find1(list).toArray();
        Object[] expected = {"1", "6"};

        Assertions.assertEquals(Arrays.toString(expected), Arrays.toString(result));
        System.out.println("Исходный список: " + list + "\n" + "Результат: " + Arrays.toString(result));
    }

    /**
     * Проверка работоспособности метода {@link FindDuplicates}
     * На вход подаётся null, ожидается выброс исключения
     */
    @Test
    public void find1_Test2() {

        ArrayList<Integer> list = new ArrayList<>();

        FindDuplicates<Integer> fD = new FindDuplicates<>();

        Throwable throwable = Assertions.assertThrows(NullPointerException.class, () -> fD.find1(list));
    }

    /**
     * Проверка работоспособности метода {@link FindDuplicates}
     */
    @Test
    public void find2_Test() {

        ArrayList<Integer> list = new ArrayList<>();
        list.add(6);
        list.add(1);
        list.add(9);
        list.add(99);
        list.add(3);
        list.add(1);
        list.add(6);
        list.add(6);

        FindDuplicates<Integer> fD = new FindDuplicates<>();
        Object[] result = fD.find2(list).toArray();
        Object[] expected = {1, 6};

        Assertions.assertEquals(Arrays.toString(expected), Arrays.toString(result));
        System.out.println("Исходный список: " + list + "\n" + "Результат: " + Arrays.toString(result));
    }

    /**
     * Проверка работоспособности метода {@link FindDuplicates}
     */
    @Test
    public void find3_Test() {

        ArrayList<Integer> list = new ArrayList<>();
        list.add(6);
        list.add(1);
        list.add(9);
        list.add(99);
        list.add(3);
        list.add(1);
        list.add(6);
        list.add(6);

        FindDuplicates<Integer> fD = new FindDuplicates<>();
        Object[] result = fD.find3(list).toArray();
        Object[] expected = {6, 1};

        Assertions.assertEquals(Arrays.toString(expected), Arrays.toString(result));
        System.out.println("Исходный список: " + list + "\n" + "Результат: " + Arrays.toString(result));
    }
}
