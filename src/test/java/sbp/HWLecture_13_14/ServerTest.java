package sbp.HWLecture_13_14;

import com.sun.net.httpserver.HttpServer;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.net.InetSocketAddress;

public class ServerTest {

    /**
     * Тест на выпадение IOException при создании сервера
     */
    @Test

    public void startServer_Test() {

        Assertions.assertThrows(IOException.class, () -> HttpServer.create(new InetSocketAddress("eee",111), 0));

    }
}
