package sbp.HWLecture_13_14;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import sbp.HWLecture_11_12.PersonDB;
import sbp.HWLecture_7_8.Person;

import java.util.List;


public class PersonHandlerTest {

    /**
     *  Тест работы клиент-серверного приложения
     *  Проверка результата ответа сервера
     */
    @Test
    public void addPerson_Test() {

        Server.startServer();
        PersonDB personDB = PersonDB.getInstance();
        personDB.dropTable();
        personDB.createTable();

        RESTClient restClient1 = new RESTClient();
        Assertions.assertEquals("{\"result\":\"ACCEPTED\"}",
                restClient1.addPerson(new Person("Test2", "Test2", 2)));
        Server.stopServer(personDB);
    }

    /**
     * Проверка метода добавления пользователя
     */
    @Test
    public void getPersons_Test() {

        Server.startServer();
        PersonDB personDB = PersonDB.getInstance();
        personDB.dropTable();
        personDB.createTable();
        personDB.addPerson(new Person("TestName", "TestCity", 10));
        personDB.addPerson(new Person("TestName2", "TestCity2", 20));
        personDB.addPerson(new Person("TestName3", "TestCity3", 30));
        RESTClient restClient1 = new RESTClient();

        Assertions.assertEquals("TestName2", restClient1.getPersonsList(-1).get(1).getName());
        Server.stopServer(personDB);
    }
}
