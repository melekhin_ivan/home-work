package sbp.HWLecture_19_20;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.web.client.HttpServerErrorException;
import sbp.HWLecture_19_20.client.RESTClient;
import sbp.HWLecture_19_20.model.Deposits;
import sbp.HWLecture_19_20.repository.DepositsDB;

import java.util.List;

public class RESTClientTest {

    private ApplicationContext context;
    private DepositsDB depositsDB;
    private RESTClient restClient;

    @BeforeEach
    public void before() {
        context = new AnnotationConfigApplicationContext("sbp");
        depositsDB = context.getBean(DepositsDB.class);
        restClient = context.getBean(RESTClient.class);
        depositsDB.createTable();
    }

    @AfterEach
    public void after() {
        depositsDB.dropTable();
    }

    /**
     *  Тест работы клиент-серверного приложения
     *  Проверка работы клиента {@link RESTClient} и обработки клиентом ответа сервера {@link sbp.HWLecture_19_20.controller.RESTController}
     */
    @Test
    public void getDeposits_Test() {

        Deposits deposits = new Deposits("ТестИмя", 10_000);
        depositsDB.addDeposits(deposits);
        Assertions.assertEquals(deposits.getOwner(),restClient.getIdDeposits(1).getOwner());
    }

    /**
     *  Тест работы клиент-серверного приложения
     *  Проверка работы клиента {@link RESTClient} и обработки клиентом ответа сервера {@link sbp.HWLecture_19_20.controller.RESTController}
     */
    @Test
    public void postDeposits_true_Test() {

        Deposits deposits = new Deposits("ТестИмя", 12_100);
        boolean result = restClient.postDeposits(deposits);
        Assertions.assertTrue(result);
    }

    /**
     *  Тест работы клиент-серверного приложения
     *  Проверка работы клиента {@link RESTClient} и обработки клиентом ответа сервера {@link sbp.HWLecture_19_20.controller.RESTController}
     */
    @Test
    public void postDeposits_throw_Test() {

        Deposits deposits = new Deposits(null, 10_000);
        Assertions.assertThrows(HttpServerErrorException.InternalServerError.class, () -> restClient.postDeposits(deposits));
    }

    /**
     *  Тест работы клиент-серверного приложения
     *  Проверка работы клиента {@link RESTClient} и обработки клиентом ответа сервера {@link sbp.HWLecture_19_20.controller.RESTController}
     */
    @Test
    public void getAllDeposits_Test() {

        List<Deposits> list = restClient.getAllDeposits();
        Assertions.assertEquals(depositsDB.findAll().toString(), list.toString());
    }
}
