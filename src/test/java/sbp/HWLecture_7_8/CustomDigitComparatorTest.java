package sbp.HWLecture_7_8;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import sbp.exceptions.CustomException;
import sbp.exceptions.WorkWithExceptions;

public class CustomDigitComparatorTest {

    /**
     * Проверка работоспособности {@link CustomDigitComparator}'
     * На вход подаются два одинаковых значения, ожидается 0
     */
    @Test
    public void compare_Test1() {

        Integer lhs = 7;
        Integer rhs = 7;

        CustomDigitComparator customDigitComparator = new CustomDigitComparator();

        int result = customDigitComparator.compare(lhs, rhs);

        Assertions.assertEquals(0, result);
    }

    /**
     * Проверка работоспособности {@link CustomDigitComparator}'
     * На вход подаются два значения, ожидается что чётное значение должно сместиться левее
     */
    @Test
    public void compare_Test2() {

        Integer lhs = 3;
        Integer rhs = 2;

        CustomDigitComparator customDigitComparator = new CustomDigitComparator();

        int result = customDigitComparator.compare(lhs, rhs);

        Assertions.assertEquals(1, result);
    }

    /**
     * Проверка работоспособности {@link CustomDigitComparator}'
     * На вход подаётся null, ожидается выброс исключения
     */
    @Test
    public void compare_Test3() {

        Integer lhs = null;
        Integer rhs = 2;

        CustomDigitComparator customDigitComparator = new CustomDigitComparator();

        WorkWithExceptions workWithExceptions = new WorkWithExceptions();
        Throwable throwable = Assertions.assertThrows(NullPointerException.class, () -> { int result = customDigitComparator.compare(lhs, rhs); });
    }
}
