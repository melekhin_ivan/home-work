package sbp.HWLecture_7_8;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import sbp.common.Utils;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.ArgumentMatchers.anyString;

public class PersonTest {

    /**
     * Проверка результата выполнения метода {@link Person}
     */
    @Test
    public void compareTo_Test() {

        List<Person> people = new ArrayList<>();
        people.add(new Person("Zoya", "Omsk", 30));
        people.add(new Person("Clark", "Omsk", 30));
        people.add(new Person("Brand", "Amsterdam", 30));
        people.add(new Person("Anton", "Amsterdam", 30));

        people.sort(Person::compareTo);
        Person person = people.get(2);

        Assertions.assertEquals("Clark", person.getName());
    }


    /**
     * Проверка работоспособности конструктора класса {@link Person}
     */
    @Test
    public void Person_Test() {

        Person person = new Person("Ivan", "Yekaterinburg", 26);

        Assertions.assertEquals("Ivan", person.getName());
    }
}
