package sbp;


import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import sbp.common.Utils;
import sbp.myUser.User;

import java.util.ArrayList;
import java.util.List;

public class UserTest {

    /**
     * Проверка результата выполнения метода в зависимости от возможных реализаций {@link User}
     * Ожидается что вызов метода .getUserID() у последнего созданного пользователя, вернёт число равное общему количеству пользователей userID
     */
    @Test
    public void getUserID_Test() {
        User user = new User("Евгений", "123@gmail.com", "d2d12d1");
        User user1 = new User("Марина", "456@gmail.com", "dc32dfasd");
        User user2 = new User("Алина", "789@gmail.com", "d312ed1");

        int expected = User.getUserID();

        Assertions.assertEquals(expected, user2.getUserID());
    }

    /**
     * Проверка результата выполнения метода в зависимости от возможных реализаций {@link User}
     * Ожидается что результат вызов метода .hashCode() будет разным у двух разных пользователей
     */
    @Test
    public void hashCode_Test_False () {
        User user1 = new User("Марина", "456@gmail.com", "dc32dfasd");
        User user2 = new User("Алина", "789@gmail.com", "d312ed1");

        boolean res = user1.hashCode() == user2.hashCode();

        Assertions.assertFalse(res);
    }

    /**
     * Проверка результата выполнения метода в зависимости от возможных реализаций {@link User}
     * Ожидается что результат вызов метода .hashCode() у двух пользователей будет одинаковым, при одинаковом параметре userMail
     */
    @Test
    public void hashCode_Test_True () {
        User user1 = new User("Марина", "456@gmail.com", "dc32dfasd");
        User user2 = new User("Алина", "456@gmail.com", "d312ed1");

        boolean res = user1.hashCode() == user2.hashCode();

        Assertions.assertTrue(res);
    }

    /**
     * Проверка результата выполнения метода в зависимости от возможных реализаций {@link User}
     * Ожидается что после использования метода .setUserSkills, список пользовательских способностей не будет пустым
     */
    @Test
    public void getAllUsers_NO_NULL() {
        User user = new User(Mockito.anyString(), Mockito.anyString(), Mockito.anyString());
        user.setUserSkills(Mockito.anyString());
        ArrayList<String> result = user.getUserSkills();
        Assertions.assertNotNull(result);
    }
}
