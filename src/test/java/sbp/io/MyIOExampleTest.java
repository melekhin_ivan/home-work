package sbp.io;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import sbp.common.Utils;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;

public class MyIOExampleTest {

    /**
     * Проверка результата выполнения метода в зависимости от существования файла
     */
    @Test
    public void workWithFileTest() throws IOException {

        MyIOExample myIOExample = new MyIOExample();

        Assertions.assertTrue(myIOExample.workWithFile("TestMIO.txt"));
        Assertions.assertFalse(myIOExample.workWithFile("Test123.txt"));
    }

    /**
     * Проверка результата выполнения метода в зависимости от существования файла
     */
    @Test
    public void copyFile_Test() throws IOException {

        MyIOExample mIO = new MyIOExample();
        try {
            Assertions.assertFalse(mIO.copyFile("File_not_exists", "File2"));
        } catch (FileNotFoundException e) {
            System.out.println("Error: " + e.getMessage());
        }

        Assertions.assertTrue(mIO.copyFile("TestMIO.txt", "TestMIO2.txt"));
    }

    /**
     * Проверка результата выполнения метода в зависимости от существования файла
     */
    @Test
    public void copyBufferedFile_Test() throws IOException {

        MyIOExample mIO = new MyIOExample();
        try {
            Assertions.assertFalse(mIO.copyBufferedFile("File_not_exists", "File2"));
        } catch (FileNotFoundException e) {
            System.out.println("Error: " + e.getMessage());
        }

        Assertions.assertTrue(mIO.copyBufferedFile("TestMIO.txt", "TestMIO2.txt"));
    }

    /**
     * Проверка результата выполнения метода в зависимости от существования файла
     */
    @Test
    public void copyFileWithReaderAndWriter_Test() throws IOException {

        MyIOExample mIO = new MyIOExample();
        try {
            Assertions.assertFalse(mIO.copyFileWithReaderAndWriter("File_not_exists", "File2"));
        } catch (FileNotFoundException e) {
            System.out.println("Error: " + e.getMessage());
        }

        Assertions.assertTrue(mIO.copyFileWithReaderAndWriter("TestMIO.txt", "TestMIO2.txt"));
    }
}
