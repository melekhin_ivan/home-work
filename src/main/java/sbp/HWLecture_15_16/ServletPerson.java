package sbp.HWLecture_15_16;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import sbp.HWLecture_11_12.PersonDB;
import sbp.HWLecture_7_8.Person;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.nio.charset.Charset;


/**
 * Сервлет для работы со страницей, отвечающей за запись новых пользователей на мероприятие,
 * путём добавления их в базу данных.
 */
@WebServlet("/person")
public class ServletPerson extends HttpServlet {


    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        getServletContext().getRequestDispatcher("/addPerson.jsp").forward(req,resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) {

        ApplicationContext context = null;
        try {
            context = new AnnotationConfigApplicationContext("sbp");
            PersonDB personDB = context.getBean(PersonDB.class);
            resp.setContentType("text/html; charset=UTF-8");

            String name = new String(req.getParameter("name").getBytes("ISO_8859_1"), Charset.forName("UTF-8"));
            String city = new String(req.getParameter("city").getBytes("ISO_8859_1"), Charset.forName("UTF-8"));
            int age = Integer.parseInt(req.getParameter("age"));
            Person person = new Person(name, city, age);
            boolean result = personDB.addPerson(person);
            resp.sendRedirect("main");
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            ((AnnotationConfigApplicationContext) context).close();
        }
    }
}
