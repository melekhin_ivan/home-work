package sbp.HWLecture_15_16;
import sbp.HWLecture_11_12.PersonDB;
import sbp.HWLecture_7_8.Person;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

/**
 * Сервлет для работы с главной страницей. На страницу выводится содержимое БД {@link PersonDB}
 */
@WebServlet("/main")
public class ServletForClub extends HttpServlet {

    private PersonDB personDB = PersonDB.getInstance();

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        List<Person> personList = personDB.findAll();
        StringBuilder builder = new StringBuilder();
        for (Person person : personList) {
            builder.append("<li>")
                    .append(person.getName())
                    .append(" из города ")
                    .append(person.getCity())
                    .append(", ")
                    .append(person.getAge())
                    .append(" лет</li>");
        }
        builder.toString();
        req.setAttribute("persons", builder);

        getServletContext().getRequestDispatcher("/main.jsp").forward(req,resp);
    }
}
