package sbp.HWLecture_13_14;

import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;
import com.sun.net.httpserver.HttpServer;
import org.json.JSONArray;
import org.json.JSONObject;
import sbp.HWLecture_11_12.PersonDB;
import sbp.HWLecture_7_8.Person;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.InetSocketAddress;
import java.nio.charset.StandardCharsets;
import java.sql.Connection;
import java.sql.DriverManager;
import java.util.concurrent.Executors;
import java.util.stream.Collectors;


/**
 * Класс для обрабоки HTTP-обменов
 * работает с базой данных {@link PersonDB}
 *
 * @author Мелехин Иван
 */
public class PersonsHandler implements HttpHandler {

    private PersonDB personDB;

    /**
     * Обрабатывает данный запрос и генерирует ответ
     * @param exchange - запрос клиента
     * @throws IOException
     */
    @Override
    public void handle(HttpExchange exchange) throws IOException {
        OutputStream outputStream = exchange.getResponseBody();

        StringBuilder htmlBuilder = new StringBuilder();
        JSONObject response = null;
        if (exchange.getRequestMethod().equalsIgnoreCase("GET")) {
            String url = exchange.getRequestURI().getPath();
            int id = -1;
            try {
                id = Integer.parseInt(url.substring(url.lastIndexOf("/") + 1, url.length()));
            } catch (NumberFormatException e) {}
           response = getMethod(id);
        } else if (exchange.getRequestMethod().equalsIgnoreCase("POST")) {
            String jsonPerson = new BufferedReader(new InputStreamReader(exchange.getRequestBody()))
                            .lines()
                            .collect(Collectors.joining());

            JSONObject jsonObject = new JSONObject(jsonPerson);

            String name = jsonObject.toMap().get("name").toString();
            String city = jsonObject.toMap().get("city").toString();
            int age = jsonObject.getInt("age");
            Person person = new Person(name, city, age);

            response = postMethod(person);
        } else if (exchange.getRequestMethod().equalsIgnoreCase("DELETE")) {
            String url = exchange.getRequestURI().getPath();
            int id = -1;
            try {
                id = Integer.parseInt(url.substring(url.lastIndexOf("/") + 1, url.length()));
            } catch (NumberFormatException e) {
                e.printStackTrace();
            }

            response = deleteMethod(id);
        } else if (exchange.getRequestMethod().equalsIgnoreCase("PUT")) {
            String url = exchange.getRequestURI().getPath();
            int id = -1;
            try {
                id = Integer.parseInt(url.substring(url.lastIndexOf("/") + 1, url.length()));
            } catch (NumberFormatException e) {
                e.printStackTrace();
            }

            String jsonPerson = new BufferedReader(new InputStreamReader(exchange.getRequestBody()))
                    .lines()
                    .collect(Collectors.joining());

            JSONObject jsonObject = new JSONObject(jsonPerson);
            String name = jsonObject.toMap().get("name").toString();
            String city = jsonObject.toMap().get("city").toString();
            int age = jsonObject.getInt("age");
            Person person = new Person(name, city, age);

            response = putMethod(person, id);
        }

        String htmlStr = htmlBuilder.toString();

        exchange.sendResponseHeaders(200, htmlStr.length());

        outputStream.write(response.toString().getBytes(StandardCharsets.UTF_8));
        outputStream.flush();
        outputStream.close();
    }

    /**
     * Метод для обработки GET запроса базы данных {@link PersonDB}
     * @return List<Person> из базы данных, или один экземпляр Person
     */
    public synchronized JSONObject getMethod(int id) {
        this.personDB = PersonDB.getInstance();
        if (id != -1) {
            return new JSONObject(personDB.findById(id));
        }
        JSONObject obj = new JSONObject();
        obj.put("persons", (Object) new JSONArray(personDB.findAll()));
        return obj;
    }

    /**
     * Метод для обработки POST запроса базы данных {@link PersonDB}
     * @param person - экземпляр класса {@link Person}
     * @return - "ACCEPTED" и
     */
    public synchronized JSONObject postMethod(Person person) {
        this.personDB = PersonDB.getInstance();
        personDB.addPerson(person);
        JSONObject rs = new JSONObject();
        rs.put("result", "ACCEPTED");
        return rs;
    }

    /**
     * Метод для обработки DELETE запроса базы данных {@link PersonDB}
     * @param id - номер пользователя
     * @return - "ACCEPTED", если метод отработал успешно
     */
    public synchronized JSONObject deleteMethod(int id) {
        JSONObject rs = new JSONObject();
        if (id == -1) {
            rs.put("result", "FAIL");
            return rs;
        }
        this.personDB = PersonDB.getInstance();
        personDB.deletePerson(id);
        rs.put("result", "ACCEPTED");
        return rs;
    }

    /**
     * Метод для обработки PUT запроса базы данных {@link PersonDB}
     * @param id - номер пользователя
     * @param person - экземпляр класса {@link Person}
     * @return - "ACCEPTED", если метод отработал успешно
     */
    public synchronized JSONObject putMethod(Person person, int id) {
        JSONObject rs = new JSONObject();
        if (id == -1) {
            rs.put("result", "FAIL");
            return rs;
        }
        this.personDB = PersonDB.getInstance();
        personDB.updatePerson(person, id);
        rs.put("result", "ACCEPTED");
        return rs;
    }
}
