package sbp.HWLecture_13_14;

import com.sun.net.httpserver.HttpServer;
import sbp.HWLecture_11_12.PersonDB;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.util.concurrent.Executors;

/**
 * Класс для создания локального сервера
 */
public class Server {

    private static final String HOSTNAME = "localhost";
    private static final int PORT = 8000;
    private static HttpServer server;

    public static void startServer() {

        try {
            server = HttpServer.create(new InetSocketAddress(HOSTNAME, PORT), 0);
        } catch (IOException e) {
            e.printStackTrace();
        }

        server.createContext("/allPersons", new PersonsHandler());

        server.setExecutor(Executors.newFixedThreadPool(5));
        server.start();
        System.out.println("Simple server started...");
    }

    public static void stopServer(PersonDB personDB) {
        server.stop(0);
        personDB.closeConn();
    }
}