package sbp.HWLecture_13_14;

import com.google.gson.Gson;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.http.*;
import org.springframework.web.client.RestTemplate;
import sbp.HWLecture_7_8.Person;

import java.io.IOException;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

/**
 * REST клиент для создания HTTP - запросов, их передачи в REST сервер
 * и обработки ответов.
 */
public class RESTClient {

    static final String URL_PERSONS = "http://localhost:8000/allPersons";
    private RestTemplate restTemplate;
    private Gson gson = new Gson();

    public RESTClient() {
        restTemplate = new RestTemplate();
    }

    /**
     * Метод для получения данных из базы
     * @return коллекция Person из базы данных
     */
    public synchronized List<Person> getPersonsList(int id){
        ArrayList<Person> persons = new ArrayList<>();
        ResponseEntity<String> response = restTemplate.getForEntity(URL_PERSONS + "/" + id, String.class);
        String answer = response.getBody();
        JSONObject obj = new JSONObject(answer);
        if (obj.getJSONArray("persons") != null) {
            Person[] res = gson.fromJson(obj.getJSONArray("persons").toString(), Person[].class);
            Collections.addAll(persons, res);
        } else {
            Person res = gson.fromJson(obj.toString(), Person.class);
            Collections.addAll(persons, res);
        }
        return persons;
    }

    /**
     * Метод добавляет экземпляр Person в БД
     * @param person - экземпляр класса {@link Person}
     * @return результат выполнения запроса
     */
    public synchronized String addPerson(Person person){
        HttpHeaders headers = new HttpHeaders();
        headers.add("Accept", MediaType.APPLICATION_JSON_VALUE);
        headers.setContentType(MediaType.APPLICATION_JSON);
        RestTemplate restTemplate = new RestTemplate();
        HttpEntity<Person> requestBody = new HttpEntity<>(person, headers);
        return restTemplate.postForObject(URL_PERSONS, requestBody, String.class);
    }

    /**
     * Перезаписывает объект с заданным ID новым объектом
     * @param id - номер пользователя
     * @param person - экземпляр класса {@link Person}
     */
    public synchronized void updatePerson(int id, Person person){
        HttpHeaders headers = new HttpHeaders();
        headers.add("Accept", MediaType.APPLICATION_JSON_VALUE);
        RestTemplate restTemplate = new RestTemplate();
        HttpEntity<Person> requestBody = new HttpEntity<>(person, headers);
        String resourceUrl = URL_PERSONS + "/" + id;
        restTemplate.exchange(resourceUrl, HttpMethod.PUT, requestBody, Void.class);
        restTemplate.getForObject(resourceUrl, String.class);
    }

    /**
     * Удаляет запись по заданному ID
     * @param id - номер пользователя
     */
    public synchronized void deletePerson(int id) {
        RestTemplate restTemplate = new RestTemplate();
        restTemplate.delete(URL_PERSONS + "/" + id);
    }
}
