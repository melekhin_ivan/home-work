package sbp.HWLecture_17_18;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import sbp.HWLecture_11_12.PersonDB;
import sbp.HWLecture_7_8.Person;

import java.util.List;


/**
 * Контроллер Spring для RESTful сервиса
 */
@RestController
@RequestMapping("/main")
public class SimpleRestController {

    @Autowired
    private PersonDB personDB;

    @GetMapping("")
    public ResponseEntity<String> getMein() {
        StringBuilder stringBuilder = new StringBuilder()
                .append("Список команд:<br>")
                .append("/getProcessing<br>")
                .append("/getProcessing/{id}<br>")
                .append("/deleteProcessing/{id}<br>")
                .append("/postProcessing<br>")
                .append("/putProcessing/{id}<br>");
        return new ResponseEntity<String>(stringBuilder.toString(), HttpStatus.OK);
    }

    /**
     * Метод для обработки GET запроса базы данных {@link PersonDB}
     */
    @GetMapping("/getProcessing")
    public ResponseEntity<List<Person>> getProcessing() {
        return new ResponseEntity<List<Person>>(personDB.findAll(), HttpStatus.OK);
    }

    /**
     * Метод для обработки GET запроса базы данных {@link PersonDB} по id пользователя
     * @param id - id персоны в БД
     */
    @GetMapping("/getProcessing/{id}")
    public ResponseEntity<Person> getPerson(@PathVariable("id") int id) {
        return new ResponseEntity<Person>(personDB.findById(id), HttpStatus.OK);
    }

    /**
     * Метод для обработки POST запроса базы данных {@link PersonDB}
     * @param person - экземпляр класса Person
     */
    @PostMapping("/postProcessing")
    public ResponseEntity<Boolean> addPerson(@RequestBody Person person) {
        boolean result = personDB.addPerson(person);
        if (result) {
            return new ResponseEntity<Boolean>(true, HttpStatus.OK);
        } else {
            return new ResponseEntity<>(false, HttpStatus.OK);
        }
    }

    /**
     * Метод для обработки DELETE запроса базы данных {@link PersonDB} по id пользователя
     * @param id - id персоны в БД
     */
    @DeleteMapping("/deleteProcessing/{id}")
    public ResponseEntity<String> deletePerson(@PathVariable("id") int id) {
        personDB.deletePerson(id);
        return new ResponseEntity<String>("Delete person success!", HttpStatus.OK);
    }

    /**
     * Метод для обработки PUT запроса базы данных {@link PersonDB} по id пользователя
     * @param id - id персоны в БД
     * @param person - экземпляр класса Person
     */
    @PutMapping("/putProcessing/{id}")
    public ResponseEntity<String> updatePerson(@PathVariable("id") int id, @RequestBody Person person) {
        personDB.updatePerson(person, id);
        return new ResponseEntity<>(HttpStatus.OK);
    }
}
