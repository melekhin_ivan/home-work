package sbp.HWLecture_17_18;

import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;
import sbp.HWLecture_7_8.Person;

import java.util.Arrays;
import java.util.List;

/**
 * REST клиент для создания запросов,
 * их передачи в REST сервер и обработки ответов.
 */
@Component
public class RestClient {

    static final String URL_PERSONS = "http://localhost:8080/main";
    private RestTemplate restTemplate;

    public RestClient() {
        restTemplate = new RestTemplate();
    }

    /**
     * Метод для отправки GET запроса
     * @param id - id персоны в БД
     * @return экземпляр класса Person
     */
    public Person getPerson(int id) {
        ResponseEntity<Person> response = restTemplate.getForEntity(URL_PERSONS + "/getProcessing/" + id, Person.class);
        Person person = restTemplate.getForObject(URL_PERSONS + "/getProcessing/" + id, Person.class);
        System.out.println(response.getBody() + " Status: " + response.getStatusCode());
        return person;
    }

    /**
     * Метод для отправки GET запроса
     * @return список всех Person, содержащишся в БД
     */
    public List<Person> getAllPersons() {
        ResponseEntity<List> response = restTemplate.getForEntity(URL_PERSONS + "/getProcessing", List.class);
        System.out.println(response.getBody() + " Status: " + response.getStatusCode());
        Person[] result = restTemplate.getForObject(URL_PERSONS + "/getProcessing", Person[].class);
        return Arrays.asList(result);
    }

    /**
     * Метод для отправки POST запроса
     * @param person - экземпляр класса Person
     * @return true, если персона успешно добавлнеа, false если идентичная запись в БД уже существует и добавление не произошло
     */
    public boolean postPerson(Person person) {
        ResponseEntity<Boolean> response = restTemplate.postForEntity(URL_PERSONS + "/postProcessing", person, Boolean.class);
        System.out.println(response.getBody() + " Status: " + response.getStatusCode());
        return response.getBody();
    }

    /**
     * Метод для отправки PUT запроса
     * @param person - экземпляр класса Person
     * @param id - id персоны в БД
     */
    public void putPerson(Person person, int id) {
        restTemplate.put(URL_PERSONS + "/putProcessing/" + id, person);
    }

    /**
     * Метод для отправки DELETE запроса
     * @param id - id персоны в БД
     */
    public synchronized void deletePerson(int id) {
        restTemplate.delete(URL_PERSONS + "/deleteProcessing/" + id);
    }
}
