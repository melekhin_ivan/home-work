package sbp.HWLecture_17_18;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;


/**
 * Класс для запуска и закрытия ресурсов сервера {@link SimpleRestController}
 */
@SpringBootApplication(scanBasePackages="sbp")
public class App {

    public static void start() {
        SpringApplication.run(App.class);
    }

    public static void stop(ApplicationContext context) {
        SpringApplication.exit(context);
    }
}
