package sbp.HWLecture_17_18.services.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 * Класс конфигурации Spring
 */
@Configuration
@PropertySource("classpath:db.properties")
public class MainConfig {

    private String jdbcUrl;

    /**
     * Сеттер-инъекция
     * @param jdbcUrl путь к базе данных
     */
    @Value("${jdbc.url}")
    public void setJdbcUrl(String jdbcUrl) {
        this.jdbcUrl = jdbcUrl;
    }

    /**
     * Бин для получения соединения с базой даных
     * @return connection
     * @throws SQLException
     */
    @Bean
    public Connection getConnection() throws SQLException {
        Connection connection = DriverManager.getConnection(jdbcUrl);
        return connection;
    }
}
