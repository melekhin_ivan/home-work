package sbp.HWLecture_19_20.logger;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.*;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

@Aspect
@Component
public class LoggerAspects {

    private Logger logger = Logger.getLogger(LoggerAspects.class.getName());

    @Pointcut("execution(* sbp.HWLecture_19_20.controller.RESTController.*(..))")
    public void restControllerPointcut() {
    }

    @Before("restControllerPointcut()")
    public void beforeMethod(JoinPoint joinPoint) {
        String methodName = joinPoint.getSignature().getName();
        List<String> args = getArgs(joinPoint);
        logger.info("Method= " + methodName + " started. args=" + args);
    }

    @AfterReturning("restControllerPointcut()")
    public void afterMethod(JoinPoint joinPoint) {
        String methodName = joinPoint.getSignature().getName();
        List<String> args = getArgs(joinPoint);
        logger.info("Method= " + methodName + " finished. args=" + args);
    }

    @AfterThrowing(value = "restControllerPointcut()", throwing = "ex")
    public void afterException(JoinPoint joinPoint, Throwable ex) {
        String methodName = joinPoint.getSignature().getName();
        List<String> args = getArgs(joinPoint);
        logger.info("ERROR!!! Failed to Method= " + methodName + " finished. args=" + args + " eror=" + ex);
    }

    public List<String> getArgs(JoinPoint joinPoint) {
        List<String> args = new ArrayList<>();
        for (int i =0; i < joinPoint.getArgs().length; ++i) {
            Object argValue = joinPoint.getArgs()[i];
            args.add("arg." + i + "=" + argValue);
        }
        return args;
    }
}
