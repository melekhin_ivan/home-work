package sbp.HWLecture_19_20.repository;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Repository;
import sbp.HWLecture_19_20.model.Deposits;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Класс для работы с базой данных DepositsDB
 */
@Repository
@PropertySource("classpath:db.properties")
public class DepositsDB {

    private static DepositsDB instance;
    private String url;

    @Value("${jdbc.url.deposit}")
    public void setUrl(String url) {
        this.url = url;
    }

    /**
     * Метод для создания таблицы
     */
    public void createTable() {

        try (Connection connection = DriverManager.getConnection(url);
             Statement statement = connection.createStatement()) {
            String sqlCreate = "create table DepositsDB (" +
                    " id INTEGER PRIMARY KEY AUTOINCREMENT," +
                    " owner varchar(32) NOT NULL," +
                    " deposit int (32) NOT NULL" +
                    " );";
            statement.executeUpdate(sqlCreate);

            System.out.println("Таблица успешно создана");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Метод для удаления таблицы
     */
    public boolean dropTable() {

        try (Connection connection = DriverManager.getConnection(url);
             Statement statement = connection.createStatement()) {
            statement.executeUpdate("drop table DepositsDB");
            System.out.println("Таблица удалена.");
            return true;
        } catch (SQLException exception) {
            exception.printStackTrace();
            return false;
        }
    }

    /**
     * Метод для добавления нового пользователя в таблицу
     *
     * @param deposits - экземпляр класса {@link Deposits}
     */
    public boolean addDeposits(Deposits deposits) {

        try (Connection connection = DriverManager.getConnection(url);
             Statement statement = connection.createStatement()) {

            String sqlAdd = String.format("insert into DepositsDB ('owner', 'deposit') values ('%s', '%d')",
                    deposits.getOwner(), deposits.getDeposit());
            if (depositsExist(deposits)) {
                System.out.println("Данная запись уже есть в таблице. Добавление не совершено.");
                return false;
            } else {
                int affectedRows = statement.executeUpdate(sqlAdd);
                System.out.println("Количество добавленных строк: " + affectedRows);
                return true;
            }
        } catch (SQLException exception) {
            exception.printStackTrace();
            return false;
        }
    }

    /**
     * Метод для проверки сущестования записи в таблице
     *
     * @param deposits - экземпляр класса {@link Deposits}
     * @return true, если запись уже существует в таблице
     */
    public boolean depositsExist(Deposits deposits) {

        try (Connection connection = DriverManager.getConnection(url);
             PreparedStatement statement = connection.prepareStatement("select * from DepositsDB where owner = ? and deposit = ?;")) {
            statement.setString(1, deposits.getOwner());
            statement.setInt(2, deposits.getDeposit());
            ResultSet resultSet = statement.executeQuery();
            if (resultSet.next()) {
                return true;
            } else {
                return false;
            }
        } catch (SQLException exception) {
            exception.printStackTrace();
            return false;
        }
    }

    /**
     * Метод для удаления пользователя
     *
     * @param id - номер вклада в таблице
     * @return true, если пользователь есть в базе и успешно удалён
     */
    public boolean deleteDeposits(int id) {

        try (Connection connection = DriverManager.getConnection(url);
             PreparedStatement statement = connection.prepareStatement("delete from DepositsDB where id = ?")) {
            statement.setInt(1, id);

            int affectedRows = statement.executeUpdate();
            if (affectedRows != 0) {
                System.out.println("Удалено строк: " + affectedRows);
                return true;
            } else {
                System.out.println("Совпадений не найдено, удаление не произведено.");
                return false;
            }
        } catch (SQLException exception) {
            exception.printStackTrace();
        }
        return false;
    }

    /**
     * Метод для отображения содержимого таблицы
     *
     * @return список всех держателей счёта, содержащихся в таблице
     */
    public List<Deposits> findAll() {

        try (Connection connection = DriverManager.getConnection(url);
             Statement statement = connection.createStatement()) {
            String sqlFindAll = "select * from DepositsDB";
            List<Deposits> personList = new ArrayList<>();

            try (ResultSet resultSet = statement.executeQuery(sqlFindAll)) {
                while (resultSet.next()) {
                    Deposits local = new Deposits(resultSet.getString("owner"), resultSet.getInt("deposit"));
                    personList.add(local);
                }
                return personList;
            }
        } catch (SQLException exception) {
            throw new RuntimeException(exception.toString());
        }
    }

    /**
     * Метод для отображения держателя счёта по id
     *
     * @param id - номер вкладчика в таблице
     * @return экземпляр класса Deposits
     */
    public Deposits findById(int id) {

        try (Connection connection = DriverManager.getConnection(url);
             Statement statement = connection.createStatement()) {
            String sqlFindById = "select * from DepositsDB where id = ?";
            Deposits deposits;
            PreparedStatement ps = connection.prepareStatement(sqlFindById);
            ps.setInt(1, id);

            try (ResultSet resultSet = ps.executeQuery()) {
                while (resultSet.next()) {
                   return new Deposits(resultSet.getString("owner"), resultSet.getInt("deposit"));
                }
            }
        } catch (SQLException exception) {
            throw new RuntimeException(exception.toString());
        }
        return null;
    }


    /**
     * Метод для изменения данных держателя счёта в таблице
     *
     * @param deposits - экземпляр класса Deposits
     * @param id - номер пользователя в таблице
     * @return true, если пользователь существует и изменён успешно
     */
    public boolean updateDeposits(Deposits deposits, int id) {
        try (Connection connection = DriverManager.getConnection(url);
             PreparedStatement statement = connection.prepareStatement("update DepositsDB set owner = ?, deposit = ? where id = ?;")) {
            statement.setString(1, deposits.getOwner());
            statement.setInt(2, deposits.getDeposit());
            statement.setInt(3, id);
            int affectedRows = statement.executeUpdate();
            if (affectedRows > 0) {
                System.out.println("Изменено строк: " + affectedRows);
                return true;
            } else {
                System.out.println("Нет пользователя с таким id");
                return false;
            }
        } catch (SQLException exception) {
            exception.printStackTrace();
            return false;
        }
    }

    /**
     * Метод выводит на экран содержимое таблицы, отсортированное по убыванию
     *
     * @param nameColumn - колонка, по которой происходит сортировка
     */
    public void seeSortPerson(String nameColumn) {

        try (Connection connection = DriverManager.getConnection(url);
             PreparedStatement statement = connection.prepareStatement("select * from DepositsDB order by " + nameColumn + " desc;")) {

            try (ResultSet resultSet = statement.executeQuery()) {
                System.out.println("owner\t\t" + "deposit");
                while (resultSet.next()) {
                    System.out.println(resultSet.getString("owner") + " " + resultSet.getString("deposit"));
                }
            }
        } catch (SQLException exception) {
            exception.toString();
        }
    }
}