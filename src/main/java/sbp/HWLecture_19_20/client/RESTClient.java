package sbp.HWLecture_19_20.client;

import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;
import sbp.HWLecture_19_20.model.Deposits;

import java.util.Arrays;
import java.util.List;

/**
 * REST клиент для создания запросов,
 * их передачи в REST сервер и обработки ответов.
 */
@Component
public class RESTClient {

    static final String URL_DEPOSITS = "http://localhost:8080/sber";
    private RestTemplate restTemplate;

    public RESTClient() {
        restTemplate = new RestTemplate();
    }

    /**
     * Метод для отправки GET запроса
     * @param id - id вклада в БД
     * @return экземпляр класса Deposits
     */
    public Deposits getIdDeposits(int id) {
        Deposits deposits = restTemplate.getForObject(URL_DEPOSITS + "/" + id, Deposits.class);
        return deposits;
    }

    /**
     * Метод для отправки GET запроса
     * @return список всех Deposits, содержащишся в БД
     */
    public List<Deposits> getAllDeposits() {
        Deposits[] depositsList = restTemplate.getForObject(URL_DEPOSITS + "/all", Deposits[].class);
        return Arrays.asList(depositsList);
    }

    /**
     * Метод для отправки POST запроса
     * @param deposits - экземпляр класса Person
     * @return true, если вкладчик успешно добавлен, false если идентичная запись в БД уже существует и добавление не произошло
     */
    public boolean postDeposits(Deposits deposits) {
        ResponseEntity<Boolean> response = restTemplate.postForEntity(URL_DEPOSITS + "/create_a_deposit", deposits, Boolean.class);
        if (response.getBody()) {
            System.out.println("Новый вклад открыт!");
        }
        return response.getBody();
    }

    /**
     * Метод для отправки DELETE запроса
     * @param id - id вкладчика в БД
     */
    public void deleteDeposits(int id) {
        restTemplate.delete(URL_DEPOSITS + "/close_deposit/" + id, Boolean.class);
    }

    /**
     * Метод для отправки PUT запроса
     * @param deposits - экземпляр класса Person
     * @param id - id вклада в БД
     */
    public void putDeposits(Deposits deposits, int id) {
        restTemplate.put(URL_DEPOSITS + "/change/" + id, deposits);
    }
}
