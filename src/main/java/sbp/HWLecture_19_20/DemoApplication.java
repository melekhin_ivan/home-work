package sbp.HWLecture_19_20;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import sbp.HWLecture_17_18.SimpleRestController;

/**
 * Класс для запуска сервера {@link SimpleRestController}
 * Сгенерированный при помощи spring initializer
 */
@SpringBootApplication
public class DemoApplication {

	public static void main(String[] args) {
		SpringApplication.run(DemoApplication.class, args);

	}
}
