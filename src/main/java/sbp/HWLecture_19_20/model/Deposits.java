package sbp.HWLecture_19_20.model;


/**
 * Класс для создания вклада
 */
public class Deposits {

    private String owner;
    private int deposit;


    /**
     * @param owner - имя держателя вклада
     * @param deposit - количество средств на счёте
     */
    public Deposits(String owner, int deposit) {
        this.owner = owner;
        this.deposit = deposit;
    }

    public Deposits() {}

    public String getOwner() {
        return owner;
    }

    public int getDeposit() {
        return deposit;
    }

    @Override
    public String toString() {
        return "Deposits{" +
                "owner='" + owner + '\'' +
                ", deposit=" + deposit +
                '}';
    }
}
