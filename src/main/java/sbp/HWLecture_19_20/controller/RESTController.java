package sbp.HWLecture_19_20.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import sbp.HWLecture_19_20.model.Deposits;
import sbp.HWLecture_19_20.repository.DepositsDB;

import java.util.List;
import java.util.Objects;

/**
 * Контроллер Spring для RESTful сервиса
 */
@RestController
@RequestMapping("/sber")
public class RESTController {

    @Autowired
    private DepositsDB depositsDB;

    @GetMapping
    public ModelAndView main() {
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("/helloBank.jsp");
        return modelAndView;
    }

    /**
     * Метод для обработки GET запроса базы данных {@link DepositsDB}
     */
    @GetMapping("/all")
    public List<Deposits> allOwner() {
        return depositsDB.findAll();
    }


    /**
     * Метод для обработки GET запроса базы данных {@link DepositsDB} по id пользователя
     * @param id - id вклада в БД
     */
    @GetMapping("/{id}")
    public Deposits ownerId(@PathVariable("id") int id) {
        if (Objects.isNull(depositsDB.findById(id))) {
            throw new IllegalStateException("Номер счёт отсутсвует");
        } else {
            return depositsDB.findById(id);
        }
    }

    /**
     * Метод для обработки POST запроса базы данных {@link DepositsDB}
     * @param deposits - экземпляр класса Deposits
     */
    @PostMapping("/create_a_deposit")
    public Boolean addOwner(@RequestBody Deposits deposits) {
        if (Objects.isNull(deposits.getOwner()) || Objects.isNull(deposits.getDeposit())) {
            throw new IllegalStateException("Сумма или имя вкладчика не могут быть пустыми");
        } else {
            return depositsDB.addDeposits(deposits);
        }
    }

    /**
     * Метод для обработки DELETE запроса базы данных {@link DepositsDB} по id пользователя
     * @param id - id вклада в БД
     */
    @DeleteMapping("/close_deposit/{id}")
    public boolean closedDeposit(@PathVariable("id") int id) {
        boolean result = depositsDB.deleteDeposits(id);
        if (result) {
            return true;
        } else {
            throw new IllegalStateException("Номер счёт отсутсвует");
        }
    }

    /**
     * Метод для обработки PUT запроса базы данных {@link DepositsDB} по id пользователя
     * @param id - id вклада в БД
     * @param deposits - экземпляр класса Deposits
     */
    @PutMapping("/change/{id}")
    public void putDeposit(@RequestBody Deposits deposits, @PathVariable("id") int id) {
        boolean result = depositsDB.updateDeposits(deposits, id);
        if (result) {
            System.out.println("Изменение успешно проведено");
        } else {
            throw new IllegalStateException("Счёт с таким id  не существует");
        }
    }
}
