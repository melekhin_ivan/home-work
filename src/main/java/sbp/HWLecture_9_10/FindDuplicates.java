package sbp.HWLecture_9_10;

import java.util.*;
import java.util.stream.Collectors;

/**
 * Данный класс реализует три различных метода, для нахождения в коллекции дубликатов (элементов, повторяющихся 1 и более раз).
 * @param <T> класс принимает любые параметры.
 */
public class FindDuplicates<T> {

    /**
     * @param listContainingDuplicates - Метод на вход принимает коллекцию (любой тип данных).
     * @return - Set коллекция элементов, имеющих дубликаты.
     */
    public Set<T> find1(Collection<T> listContainingDuplicates) {

        if (listContainingDuplicates.isEmpty()) {
            throw new NullPointerException("На вход должна подаваться не пустая коллекция.");
        }

        Set<T> setToReturn = new HashSet<>();
        Set<T> set1 = new HashSet<>();

        for (T yourElement : listContainingDuplicates) {
            if (!set1.add(yourElement))
            {
                setToReturn.add(yourElement);
            }
        }
        return setToReturn;
    }

    /**
     * @param listContainingDuplicates - Метод на вход принимает коллекцию (любой тип данных).
     * @return - ArrayList коллекция элементов, имеющих дубликаты.
     */
    public ArrayList<T> find2(Collection<T> listContainingDuplicates) {

        if (listContainingDuplicates.isEmpty()) {
            throw new NullPointerException("На вход должна подаваться не пустая коллекция.");
        }

        ArrayList<T> list1 = new ArrayList<>();
        ArrayList<T> listToReturn = new ArrayList<>();

        for (T yourElement : listContainingDuplicates) {
            if (list1.contains(yourElement)) {
                if (!listToReturn.contains(yourElement)) {
                    listToReturn.add(yourElement);
                }
            }
            list1.add(yourElement);
        }
        return listToReturn;
    }

    /**
     * @param listContainingDuplicates - Метод на вход принимает коллекцию (любой тип данных).
     * @return - List коллекция элементов, имеющих дубликаты.
     */
    public List<T> find3(Collection<T> listContainingDuplicates) {

        if (listContainingDuplicates.isEmpty()) {
            throw new NullPointerException("На вход должна подаваться не пустая коллекция.");
        }

        List<T> result = new ArrayList<>();

        return result = listContainingDuplicates.stream()
                .filter(e -> Collections.frequency(listContainingDuplicates, e) > 1)
                .distinct()
                .collect(Collectors.toList());
    }
}
