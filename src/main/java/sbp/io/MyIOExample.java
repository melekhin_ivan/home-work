package sbp.io;

import java.io.*;

public class MyIOExample
{
    /**
     * Создать объект класса {@link File}, проверить существование и чем является (фалй или директория).
     * Если сущность существует, то вывести в консоль информацию:
     *      - абсолютный путь
     *      - родительский путь
     * Если сущность является файлом, то вывести в консоль:
     *      - размер
     *      - время последнего изменения
     * Необходимо использовать класс {@link File}
     * @param fileName - имя файла
     * @return - true, если файл успешно создан
     */
    public boolean workWithFile(String fileName) throws IOException {

        File newFile = new File(fileName);

        boolean exists = newFile.exists();
        boolean checkFile = newFile.isFile();
        boolean checkDirectory = newFile.isDirectory();

        if (exists) {
            System.out.println("Абсолютный путь: " + newFile.getAbsolutePath());
            System.out.println("Родительский путь: " + newFile.getParent());
        }

        if (checkFile) {
            System.out.println("Размер файла: " + newFile.length());
            System.out.println("Время последнего изменения файла: " + newFile.lastModified() + "мс.");
        }
        return exists;
    }

    /**
     * Метод должен создавать копию файла
     * Необходимо использовать IO классы {@link java.io.FileInputStream} и {@link java.io.FileOutputStream}
     * @param sourceFileName - имя исходного файла
     * @param destinationFileName - имя копии файла
     * @return - true, если файл успешно скопирован
     */
    public boolean copyFile(String sourceFileName, String destinationFileName) throws IOException {

        FileInputStream inputStream = null;
        FileOutputStream outputStream = null;
        try {
            inputStream = new FileInputStream(sourceFileName);
            outputStream = new FileOutputStream(destinationFileName);

            int bytesReader = inputStream.read();
            while (bytesReader != -1) {
                outputStream.write(bytesReader);
                bytesReader = inputStream.read();
            }
        } catch (IOException e) {
            System.out.println("IOException.");
            e.printStackTrace();
            return false;
        } finally {
            if (inputStream != null && outputStream != null) {
                inputStream.close();
                outputStream.close();
            }
        }
        return true;
    }

    /**
     * Метод должен создавать копию файла
     * Необходимо использовать IO классы {@link java.io.BufferedInputStream} и {@link java.io.BufferedOutputStream}
     * @param sourceFileName - имя исходного файла
     * @param destinationFileName - имя копии файла
     * @return - true, если файл успешно скопирован
     */
    public boolean copyBufferedFile(String sourceFileName, String destinationFileName) throws IOException {

        InputStream inputStream = null;
        OutputStream outputStream = null;
        try {
            inputStream = new FileInputStream(sourceFileName);
            outputStream = new FileOutputStream(destinationFileName);

            BufferedInputStream bufferedInputStream = new BufferedInputStream(inputStream);
            BufferedOutputStream bufferedOutputStream = new BufferedOutputStream(outputStream);
            int len;
            while ((len = bufferedInputStream.read()) != -1) {
                bufferedOutputStream.write(len);
            }
            bufferedOutputStream.flush();

        } catch (IOException e) {
            System.out.println("IOException.");
            e.printStackTrace();
            return false;
        } finally {
            if (inputStream != null && outputStream != null) {
                inputStream.close();
                outputStream.close();
            }
        }
        return true;
    }

    /**
     * Метод должен создавать копию файла
     * Необходимо использовать IO классы {@link java.io.FileReader} и {@link java.io.FileWriter}
     * @param sourceFileName - имя исходного файла
     * @param destinationFileName - имя копии файла
     * @return - true, если файл успешно скопирован
     */
    public boolean copyFileWithReaderAndWriter(String sourceFileName, String destinationFileName) throws IOException {

        FileReader fileReader = null;
        FileWriter fileWriter = null;
        try {
            fileReader = new FileReader(sourceFileName);
            fileWriter = new FileWriter(destinationFileName);

            int len = fileReader.read();
            while(len != -1) {
                fileWriter.write(len);
                len = fileReader.read();
            }
        } catch (IOException e) {
            System.out.println("IOException.");
            e.printStackTrace();
            return false;
        } finally {
            if (fileReader != null && fileWriter != null) {
                fileReader.close();
                fileWriter.close();
            }
        }
        return true;
    }
}
