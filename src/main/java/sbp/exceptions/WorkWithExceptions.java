package sbp.exceptions;

public class WorkWithExceptions
{
    /**
     * В данном методе необходимо вызвать методы throwCheckedException и throwUncheckedException.
     * Все исключения должны быть обработаны
     * Необходимо вывести описание exception и stacktrace в консоль
     * Впойманное исключение необходимо упаковать в кастомное исключение и пробросить выше
     * Перед завершением работы метода обязательно необходимо вывести в консоль сообщение "Finish"
     */
    public void exceptionProcessing() throws Exception {

        try {
            throwCheckedException();
        } catch (Exception e) {
            System.out.println("Error: " + e.getMessage());
            e.printStackTrace(System.out);
            throw new CustomException(e);
        } finally {
            try {
                throwUncheckedException();
            } catch (Exception e1) {
                System.out.println(e1.getMessage());
                e1.printStackTrace(System.out);
                throw new CustomException(e1);
            } finally {
                System.out.println("Finish");
            }
        }
    }

    /**
     * (* - необязательно) Доп. задание.
     * Выписать в javadoc (здесь) - все варианты оптимизации и устранения недочётов метода
     * @throws IllegalStateException
     * @throws Exception
     * @throws RuntimeException
     * @throws NullPointerException
     */
    public void hardExceptionProcessing() throws IllegalStateException, Exception, RuntimeException {
        System.out.println("Start");
        try {
            System.out.println("Step 1");
            throw new IllegalArgumentException();
        } catch (IllegalArgumentException e) {
            System.out.println("Catch IllegalArgumentException");
            throw new RuntimeException("Step 2");
        } catch (NullPointerException e) {
            System.out.println("Catch NullPointerException");
            throw new NullPointerException("Step 3");
        } catch (RuntimeException e) {
            System.out.println("Catch RuntimeException");
            throw new RuntimeException("Step 4");
        } finally
        {
            System.out.println("Step finally");
            throw new RuntimeException("From finally");
        }
    }

    private void throwCheckedException() throws Exception
    {
        throw new Exception("Checked exception");
    }

    private void throwUncheckedException()
    {
        throw new RuntimeException("Unchecked exception");
    }
}
