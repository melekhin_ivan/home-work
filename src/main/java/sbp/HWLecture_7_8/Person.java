package sbp.HWLecture_7_8;

public class Person implements Comparable<Person> {

    /**
     * Класс Person предназначен для создания пользователей. Содержит поля:
     * @param name - имя пользователя
     * @param city - город
     * @param age - возраст
     * Следующие методы были переопределены: {@link #toString()} {@link #compareTo(Person o)}
     */
    private String name;
    private String city;
    private int age;

    public Person(){}

    public Person(String name, String city, int age) {
        this.name = name;
        this.city = city;
        this.age = age;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getName() {
        return name;
    }

    public String getCity() {
        return city;
    }

    public int getAge() {
        return age;
    }

    @Override
    public String toString() {
        return "Person{" +
                "name='" + name + '\'' +
                ", city='" + city + '\'' +
                ", age=" + age +
                '}';
    }

    /**
     * Переопределённый метод сравнивает двух пользователей и сортирует их сначалп по полю city, затем по полю name
     *
     * @param other - другой пользователь
     * @return сортировка сначала по городу, затем по имени
     * @throws NullPointerException если переданы null параметры
     */
    @Override
    public int compareTo(Person other) {

        if (name == null || city == null) {
            throw new NullPointerException("Ожидается не null значение.");
        }

        int c = this.city.compareToIgnoreCase(other.city);

        if (c != 0) {
            return c;
        } else {
            return this.name.compareToIgnoreCase(other.name);
        }
    }
}
