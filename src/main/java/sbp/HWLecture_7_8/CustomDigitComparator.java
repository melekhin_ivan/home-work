package sbp.HWLecture_7_8;

import java.util.Comparator;

/**
 * Кастомный сомпаратор для Integer.
 * Принимает не нулевые значения, сортирует числа по принципу:
 * - сначала чётные, затем нечётные
 * - по возрастанию
 */
public class CustomDigitComparator implements Comparator<Integer> {


    /**
     * Этот переопределённый метод сравнивает два значения типа Integer для сортировки
     * по возрастанию и от чётных к нечётным числам.
     *
     * @param lhs первое число для сравнения
     * @param rhs второе число для сравнения
     * @return -1, 0 или 1, если первый аргумент, соотвественно меньше, равен или больше второго
     * @throws NullPointerException если переданы null параметры
     */
    @Override
    public int compare(Integer lhs, Integer rhs) {

        if (lhs == null || rhs == null) {
            throw new NullPointerException("Ожидается не null значение.");
        }

        int result = lhs.compareTo(rhs);

        if (lhs % 2 == rhs % 2) {
            return result;
        }

        if (lhs % 2 > rhs % 2) {
            return 1;
        } else {
            return -1;
        }
    }
}
