package sbp.myUser;

import sbp.exceptions.WorkWithExceptions;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;



public class User {
    /**
     * Класс User предназначен для создания пользователей. Содержит поля:
     * @param userName - имя пользователя
     * @param userMail - адрес электронной почты
     * @param userPas - пароль пользователя
     * @param userOS - операционная система, используемая пользователем
     * @param userSkills - навыки и умения пользователя в сфере IT
     * @param dateReg - дата регистрации
     * @param userID - уникальный идентификатор пользователя
     * Следующие методы были переопределены: {@link #equals(Object)} {@link #toString()} {@link #hashCode()}
     */
    private String userName;
    private String userMail;
    private String userPas;
    private String userOS;
    private ArrayList<String> userSkills = new ArrayList<>();
    final private Date dateReg;
    private static int userID = 0;

    public User(String userName, String userMail, String userPas) {
        this.userName = userName;
        this.userMail = userMail;
        this.userPas = userPas;
        dateReg = new Date();
        userID++;
    }

    public static int getUserID() {
        return userID;
    }

    public String getUserName() {
        return userName;
    }
    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getUserOS() {
        return userOS;
    }
    public void setUserOS(String userOS) {
        this.userOS = userOS;
    }

    public Date getDateReg() {
        return dateReg;
    }

    public ArrayList<String> getUserSkills() {
        return userSkills;
    }
    public void setUserSkills(String ... skills) {
        Collections.addAll(userSkills, skills);
    }

    public String getUserPas() {
        return userPas;
    }

    public String getUserMail() {
        return userMail;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        User user = (User) o;
        return userMail.equals(user.userMail);
    }

    @Override
    public String toString() {
        return "Пользователь " + userName + "\nПочта " + userMail + "\nID " + userID;
    }

    @Override
    public int hashCode() {
        int result = userMail == null ? 0 : userMail.hashCode();
        result = 73 * result + userID;
        return result;
    }
}
