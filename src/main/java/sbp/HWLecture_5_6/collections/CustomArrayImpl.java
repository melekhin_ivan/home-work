package sbp.HWLecture_5_6.collections;

import java.util.Collection;

/**
 * Динамический массив. Можно хранить любые объекты.
 */
public class CustomArrayImpl<T> implements CustomArray<T> {

    private int capacity = 10;
    private Object[] array;
    private int size = 0;

    /**
     * Стандартный конструктор
     */
    public CustomArrayImpl() {
        array = new Object[capacity];
    }

    /**
     * @param capacity - переданная в конструктор ёмкость массива
     */
    public CustomArrayImpl(int capacity) {
        this.capacity = capacity;
        array = new Object[capacity];
    }

    /**
     * @param c - переданная в конструктор коллекция
     */
    public CustomArrayImpl(Collection<T> c) {
        size = c.size();
        ensureCapacity(size);
        Object[] buf = c.toArray();
        array = new Object[capacity];
        System.arraycopy(buf, 0, array, 0, size);
    }


    /**
     * @return размер массива
     */
    @Override
    public int size() {
        return size;
    }

    /**
     * @return true, если массив не содержит объектов и false, если содержит
     */
    @Override
    public boolean isEmpty() {
        return size <= 0;
    }

    /**
     * Добавляет одиночный элемент
     * @param item - объект
     * @return true
     */
    @Override
    public boolean add(T item) {
        ensureCapacity(size);
        array[size] = item;
        size++;
        return true;
    }

    /**
     * Добавляет все объекты
     * @param items - массив объектов
     * @return true
     */
    @Override
    public boolean addAll(T[] items) {
        for (int i = 0; i < items.length; i++) {
            if (items[i] == null) {
                throw new IllegalArgumentException("Как минимум один из элементов, имеет недопустимый параметр null");
            }
        }

        ensureCapacity(items.length + size);
        System.arraycopy(items, 0, array, size, items.length);
        size = size + items.length;
        return true;
    }

    /**
     * Добавляет все объекты
     * @param items - коллекция объектов
     * @return true
     */
    @Override
    public boolean addAll(Collection<T> items) {
        for (int i = 0; i < items.size(); i++) {
            if (items.contains(null)) {
                throw new IllegalArgumentException("Как минимум один из элементов, имеет недопустимый параметр null");
            }
        }

        ensureCapacity(size + items.size());
        Object[] buf = items.toArray();
        System.arraycopy(buf, 0, array, size, items.size());
        size += buf.length;
        return true;
    }

    /**
     * Добавляет объекты в текущее место в массиве.
     * @param index - индекс
     * @param items - массив объектов для вставки
     * @return true
     */
    @Override
    public boolean addAll(int index, T[] items) {
        if (index >= size || index < 0) {
            throw new ArrayIndexOutOfBoundsException();
        }
        for (int i = 0; i < items.length; i++) {
            if (items[i] == null) {
                throw new IllegalArgumentException("Как минимум один из элементов, имеет недопустимый параметр null");
            }
        }

        ensureCapacity(items.length + size);
        Object[] buf = array.clone();
        size += items.length;
        array = new Object[capacity];
        System.arraycopy(buf, 0, array, 0, index);
        System.arraycopy(items, 0, array, index, items.length);
        System.arraycopy(buf, index, array, items.length + index, (size - index - items.length));
        return true;
    }

    /**
     * @param index - индекс
     * @return возвращает объект из массива по указанному индексу
     */
    @Override
    public T get(int index) {
        if (index >= size || index < 0) {
            throw new ArrayIndexOutOfBoundsException();
        }
        return (T) array[index];
    }

    /**
     * @param index - индекс
     * @param item - объект
     * @return - заменяет объект в массиве по заданному индексу на переданный в метод объект
     */
    @Override
    public T set(int index, T item) {
        if (index >= size || index < 0) {
            throw new ArrayIndexOutOfBoundsException();
        }
        return (T) (array[index] = item);
    }

    /**
     * Удаляет элемент по индексу
     * @param index - индекс
     */
    @Override
    public void remove(int index) {
        if (index >= size || index < 0) {
            throw new ArrayIndexOutOfBoundsException();
        }
        while (index < array.length - 1) {
            array[index] = array[++index];
        }
        Object[] buf = array.clone();
        size--;
        array = new Object[capacity];
        System.arraycopy(buf, 0, array, 0, (buf.length - 1));
    }

    /**
     * Удаляет элемент по значению. Удаляет только первое вхождение элемента.
     * @param item - item
     * @return true, если объект удалён. false - если массив не содержит указанного объекта
     */
    @Override
    public boolean remove(T item) {
        int index = indexOf(item);
        if (index == -1) {
            return false;
        }
        while (index < array.length - 1) {
            array[index] = array[++index];
        }
        Object[] buf = array.clone();
        size--;
        array = new Object[capacity];
        System.arraycopy(buf, 0, array, 0, (buf.length - 1));
        return true;
    }

    /**
     * @param item - item
     * @return true, если массив содержит переданный объект. false - если массив не содержит указанного объекта
     */
    @Override
    public boolean contains(T item) {
        for (int i = 0; i < size; i++) {
            if (array[i].equals(item)) {
                return true;
            }
        }
        return false;
    }

    /**
     * @param item - переданный объект
     * @return возвращает индекс указанного объекта или -1, если массив не сордержит переданный объект
     */
    @Override
    public int indexOf(T item) {
        for (int i = 0; i < size; i++) {
            if (array[i].equals(item)) {
                return i;
            }
        }
        return -1;
    }

    /**
     * При необходимости метод увеличивает текущую емкость для хранения новых элементов.
     * @param newElementsCount - количество новых элементов
     */
    @Override
    public void ensureCapacity(int newElementsCount) {
        if (newElementsCount >= getCapacity() - 1) {
            Object[] buf = array.clone();
            capacity =(int) (capacity + newElementsCount * 1.5);
            this.array = new Object[capacity];
            System.arraycopy(buf, 0, array, 0, buf.length);
        }
    }

    /**
     * Метод возвращает текущую ёмкость динамического массива
     * @return capacity
     */
    @Override
    public int getCapacity() {
        return capacity;
    }

    /**
     * Метод реверсирует объекты в динамическом массиве
     */
    @Override
    public void reverse() {
        for (int i = 0; i < size / 2; i++) {
            Object temp = array[i];
            array[i] = array[size - 1 - i];
            array[size - 1 - i] = temp;
        }
    }

    /**
     * @return Представляет динамический массив в виде строки для читаемого вида при выводе в консоль
     */
    @Override
    public String toString() {
        String info = "";
        int i = 0;
        while (i < size) {
            info = info + array[i] + " ";
            i++;
        }
        return "[ " + info + "]";
    }

    /**
     * @return Получить копию текущего массива.
     */
    @Override
    public Object[] toArray() {
        return array;
    }
}
