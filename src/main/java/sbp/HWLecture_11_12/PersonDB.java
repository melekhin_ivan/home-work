package sbp.HWLecture_11_12;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import sbp.HWLecture_7_8.Person;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Класс для работы с базой данных PersonDB
 */
@Repository
public class PersonDB implements AutoCloseable {

    private static PersonDB instance;

    private String url;
    private Connection connection;

    @Autowired
    public void setConnection(Connection connection) {
        this.connection = connection;
    }

    public PersonDB() {
    }

    public static PersonDB getInstance() {
        if (instance == null) {
            instance = new PersonDB();
        }
        return instance;
    }

    public void closeConn() {
        try {
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /**
     * Метод для создания таблицы
     */
    public void createTable() {

        try (Statement statement = connection.createStatement()) {
            String sqlCreate = "create table PersonDB (" +
                    " number_person INTEGER PRIMARY KEY AUTOINCREMENT," +
                    " name varchar(32) NOT NULL," +
                    " city varchar(32) NOT NULL," +
                    " age int (2) NOT NULL" +
                    " );";
            statement.executeUpdate(sqlCreate);

            System.out.println("Таблица успешно создана");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Метод для удаления таблицы
     */
    public boolean dropTable() {

        try (Statement statement = connection.createStatement()) {
            statement.executeUpdate("drop table PersonDB");
            System.out.println("Таблица удалена.");
            return true;
        } catch (SQLException exception) {
            exception.printStackTrace();
            return false;
        }
    }

    /**
     * Метод для добавления нового пользователя в таблицу
     *
     * @param person - экземпляр класса {@link Person}
     */
    public boolean addPerson(Person person) {

        try (Statement statement = connection.createStatement()) {

            String sqlAdd = String.format("insert into PersonDB ('name', 'city', 'age') values ('%s', '%s', '%d')",
                    person.getName(), person.getCity(), person.getAge());

            if (personExist(person)) {
                System.out.println("Данная запись уже есть в таблице. Добавление не совершено.");
                return false;
            } else {
                int affectedRows = statement.executeUpdate(sqlAdd);
                System.out.println("Количество добавленных строк: " + affectedRows);
                return true;
            }
        } catch (SQLException exception) {
            exception.printStackTrace();
            return false;
        }
    }

    /**
     * Метод для проверки сущестования записи в таблице
     *
     * @param person - экземпляр класса {@link Person}
     * @return true, если запись уже существует в таблице
     */
    public boolean personExist(Person person) {

        try (PreparedStatement statement = connection.prepareStatement("select * from PersonDB where name = ? and city = ? and age = ?;")) {
            statement.setString(1, person.getName());
            statement.setString(2, person.getCity());
            statement.setInt(3, person.getAge());
            ResultSet resultSet = statement.executeQuery();
            if (resultSet.next()) {
                return true;
            } else {
                return false;
            }
        } catch (SQLException exception) {
            exception.printStackTrace();
            return false;
        }
    }

    /**
     * Метод для удаления пользователя
     *
     * @param id - номер пользователя в таблице
     */
    public void deletePerson(int id) {

        try (PreparedStatement statement = connection.prepareStatement("delete from PersonDB where number_person = ?")) {
            statement.setInt(1, id);

            int affectedRows = statement.executeUpdate();
            if (affectedRows != 0) {
                System.out.println("Удалено строк: " + affectedRows);
            } else {
                System.out.println("Совпадений не найдено, удаление не произведено.");
            }
        } catch (SQLException exception) {
            exception.printStackTrace();
        }
    }

    /**
     * Метод для отображения содержимого таблицы
     *
     * @return список всех пользователей, содержпщихся в таблице
     */
    public List<Person> findAll() {

        try (Statement statement = connection.createStatement()) {
            String sqlFindAll = "select * from PersonDB";
            List<Person> personList = new ArrayList<>();

            try (ResultSet resultSet = statement.executeQuery(sqlFindAll)) {
                while (resultSet.next()) {
                    Person local = new Person(resultSet.getString("name"), resultSet.getString("city"), resultSet.getInt("age"));
                    personList.add(local);
                }
                return personList;
            }
        } catch (SQLException exception) {
            throw new RuntimeException(exception.toString());
        }
    }

    /**
     * Метод для отображения пользователя
     *
     * @param id - номер пользователя в таблице
     * @return экземпляр класса Person
     */
    public Person findById(int id) {

        try (Statement statement = connection.createStatement()) {
            String sqlFindById = "select * from PersonDB where number_person = ?";
            Person person;
            PreparedStatement ps = connection.prepareStatement(sqlFindById);
            ps.setInt(1, id);

            try (ResultSet resultSet = ps.executeQuery()) {
                while (resultSet.next()) {
                   return new Person(resultSet.getString("name"), resultSet.getString("city"), resultSet.getInt("age"));
                }
            }
        } catch (SQLException exception) {
            throw new RuntimeException(exception.toString());
        }
        return null;
    }


    /**
     * Метод для изменения пользователя в таблице
     *
     * @param person - экземпляр класса Person
     * @param id - номер пользователя в таблице
     */
    public void updatePerson(Person person, int id) {
        try (PreparedStatement statement = connection.prepareStatement("update PersonDB set name = ?, city = ?, age = ? where number_person = ?;")) {
            statement.setString(1, person.getName());
            statement.setString(2, person.getCity());
            statement.setInt(3, person.getAge());
            statement.setInt(4, id);
            int affectedRows = statement.executeUpdate();
            System.out.println("Изменено строк: " + affectedRows);
        } catch (SQLException exception) {
            exception.printStackTrace();
        }
    }

    /**
     * Метод выводит на экран содержимое таблицы, отсортированное по убыванию
     *
     * @param nameColumn - колонка, по которой происходит сортировка
     */
    public void seeSortPerson(String nameColumn) {

        try (PreparedStatement statement = connection.prepareStatement("select * from PersonDB order by " + nameColumn + " desc;")) {

            try (ResultSet resultSet = statement.executeQuery()) {
                System.out.println("name\t\t" + "city\t\t" + "age");
                while (resultSet.next()) {
                    System.out.println(resultSet.getString("name") + " " + resultSet.getString("city") + " " + resultSet.getString("age"));
                }
            }
        } catch (SQLException exception) {
            exception.toString();
        }
    }

    @Override
    public void close() throws Exception {
        connection.close();
    }
}