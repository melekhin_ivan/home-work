package sbp.HWLecture_11_12;

import java.util.Iterator;

/**
 * Класс для итерирования по двумерному массиву.
 * @param <T> параметризованный тип
 */
public class ArrayIterator<T> implements Iterator<T> {

    private T[][] m;
    private int i, j;

    public ArrayIterator(T[][] m) {
        this.m = m;
    }

    @Override
    public boolean hasNext() {
        return i < m.length && j < m[i].length;
    }

    @Override
    public T next() {
        T r = m[i][j++];
        if (j >= m[i].length) {
            i++;
            j = 0;
        }
        return r;
    }
}


class Main {

    public static void main(String[] args) {

        String[][] array = new String[][]{{"1"}, {"2","3","4","5",},{"6","7"},{"8","9","10","11","12",}};
        ArrayIterator i = new ArrayIterator(array);
        while (i.hasNext()) {
            System.out.println(i.next());
        }
    }
}