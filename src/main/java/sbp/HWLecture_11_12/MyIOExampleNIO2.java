package sbp.HWLecture_11_12;

import java.io.*;
import java.nio.file.FileAlreadyExistsException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.Calendar;
import java.util.Date;

public class MyIOExampleNIO2 {

    /**
     * Создать объект класса {@link Files}, проверить существование и чем является (фалй или директория).
     * Если сущность существует, то вывести в консоль информацию:
     *      - абсолютный путь
     *      - родительский путь
     * Если сущность является файлом, то вывести в консоль:
     *      - размер
     *      - время последнего изменения
     * Необходимо использовать класс {@link Files}
     * @param fileName - имя файла
     * @return - true, если файл успешно создан
     */
    public boolean workWithFile(String fileName) throws IOException {

        Path path = Paths.get(fileName);

        try {
            Files.createFile(path);
        } catch (IOException ex) {
            ex.printStackTrace();
            return false;
        }

        boolean exists = Files.exists(path);
        boolean isFile = Files.isRegularFile(path);

        if (exists) {
            System.out.println("Абсолютный путь: " + path.toAbsolutePath());
            System.out.println("Родительский путь: " + path.toAbsolutePath().getParent());
        }

        if (isFile) {
            BasicFileAttributes basicFileAttributes = Files.readAttributes(path, BasicFileAttributes.class);
            System.out.println("Размер файла = " + Files.size(path) + " byte");
            System.out.println("Дата создания = " + basicFileAttributes.creationTime());
        }
        return true;
    }


    /**
     * Метод должен создавать копию файла
     * Необходимо использовать IO классы {@link java.io.InputStream} и {@link java.io.OutputStream}
     * Необходимо использовать NIO
     * @param sourceFileName - имя исходного файла
     * @param destinationFileName - имя копии файла
     * @return - true, если файл успешно скопирован
     */
    public boolean copyFile(String sourceFileName, String destinationFileName) throws IOException {

        try (InputStream inputStream = Files.newInputStream(Paths.get(sourceFileName));
             OutputStream outputStream = Files.newOutputStream(Paths.get(destinationFileName))) {

            int bytesReader = inputStream.read();
            while (bytesReader != -1) {
                outputStream.write(bytesReader);
                bytesReader = inputStream.read();
            }
        } catch (IOException e) {
            System.out.println("IOException.");
            e.printStackTrace();
            return false;
        }
        return true;
    }

    /**
     * Метод должен создавать копию файла
     * Необходимо использовать IO классы {@link java.io.BufferedInputStream} и {@link java.io.BufferedOutputStream}
     * Необходимо использовать NIO
     * @param sourceFileName - имя исходного файла
     * @param destinationFileName - имя копии файла
     * @return - true, если файл успешно скопирован
     */
    public boolean copyBufferedFile(String sourceFileName, String destinationFileName) throws IOException {

        try (InputStream inputStream = Files.newInputStream(Paths.get(sourceFileName));
             OutputStream outputStream = Files.newOutputStream(Paths.get(destinationFileName))) {

            BufferedInputStream bufferedInputStream = new BufferedInputStream(inputStream);
            BufferedOutputStream bufferedOutputStream = new BufferedOutputStream(outputStream);
            int len;
            while ((len = bufferedInputStream.read()) != -1) {
                bufferedOutputStream.write(len);
            }
            bufferedOutputStream.flush();

        } catch (IOException e) {
            System.out.println("IOException.");
            e.printStackTrace();
            return false;
        }
        return true;
    }

    /**
     * Метод должен создавать копию файла
     * Необходимо использовать IO классы {@link java.io.BufferedInputStream} и {@link java.io.BufferedOutputStream}
     * Необходимо использовать NIO
     * @param sourceFileName - имя исходного файла
     * @param destinationFileName - имя копии файла
     * @return - true, если файл успешно скопирован
     */
    public boolean copyFileWithReaderAndWriter(String sourceFileName, String destinationFileName) throws IOException {

        try (BufferedReader fileReader = Files.newBufferedReader(Paths.get(sourceFileName));
             BufferedWriter fileWriter = Files.newBufferedWriter(Paths.get(destinationFileName))) {

            int len = fileReader.read();
            while(len != -1) {
                fileWriter.write(len);
                len = fileReader.read();
            }
        } catch (IOException e) {
            System.out.println("IOException.");
            e.printStackTrace();
            return false;
        }
        return true;
    }
}
